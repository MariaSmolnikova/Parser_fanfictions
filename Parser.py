import requests
from bs4 import BeautifulSoup
# -*- coding: utf-8 -*-


def get_html(url):
    r = requests.get(url)
    return r.text


def get_all_links(html, host):
    soup = BeautifulSoup(html, 'lxml')
    urls = soup.find('article', class_='article').find_all('a', class_='visit-link')

    links = []
    for i in urls:
        a = i.get('href')
        link = host + a
        links.append(link)
    return links


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    text = soup.find('div', class_="part_text urlize public_beta_disabled")
    if text is None:
        text = soup.find('div', class_="part_text urlize public_beta")
    return text.text.strip()


def main():
    host = 'https://ficbook.net'
    f_read = open('urls_Twilight.txt', 'r', encoding='utf-8')
    f_write = open('dump_Twilight.txt', 'a', encoding='utf-8')
    for url in f_read:
        print(url)
        all_links = get_all_links(get_html(url), host)
        for link in all_links:
            data = get_page_data(get_html(link))
            f_write.write(data)
    f_write.close()
if __name__ == '__main__':
    main()

